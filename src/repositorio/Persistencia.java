/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package repositorio;

import hospedajes.modelo.Almacen;
import hospedajes.modelo.Cliente;
import hospedajes.modelo.Habitacion;
import hospedajes.modelo.Mobiliario;
import hospedajes.modelo.Ubicacion;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.nio.file.Path;
import java.nio.file.Paths;
/**
 *
 * @author sistemas
 */
public class Persistencia {
    public static Map<String,Ubicacion> ubicaciones = new HashMap<>();
    public static Map<String,Almacen> almacenes = new HashMap<>();
    public static Map<String,Habitacion> habitaciones = new HashMap<>();
    public static Map<String,Mobiliario> mobiliarios = new HashMap<>();
    public static Map<Integer,Cliente> clientes = new HashMap<>();

    public static void guardarUbicaciones(){
        _guardarObjeto(ubicaciones,"ubicaciones.dat");
    }
    public static void guardarAlmacenes(){
        _guardarObjeto(almacenes,"almacenes.dat");
    }
    public static void guardarHabitaciones(){
        _guardarObjeto(habitaciones,"habitaciones.dat");
    }
    public static void guardarMobiliarios(){
        _guardarObjeto(mobiliarios,"mobiliarios.dat");
    }
    public static void guardarClientes(){
        _guardarObjeto(clientes,"clientes.dat");
    }
    public static void recuperarUbicaciones(){
        Object recuperado = _recuperarObjeto("ubicaciones.dat");
        if(recuperado!=null){
            ubicaciones = (Map<String,Ubicacion>) recuperado;
        }        
    }
    public static void recuperarAlmacenes(){
        Object recuperado = _recuperarObjeto("almacenes.dat");
        if(recuperado!=null){
            almacenes = (Map<String,Almacen>) recuperado;
        }  
    }
    public static void recuperarHabitaciones(){        
        Object recuperado = _recuperarObjeto("habitaciones.dat");
        if(recuperado!=null){
            habitaciones = (Map<String,Habitacion>) recuperado;
        }
    }
    public static void recuperarMobiliarios(){
        Object recuperado = _recuperarObjeto("mobiliarios.dat");
        if(recuperado!=null){
            mobiliarios = (Map<String,Mobiliario>) recuperado;
        }
    }
    public static void recuperarClientes(){
        //clientes = (Map<Integer,Cliente>)_recuperarObjeto("clientes.dat");
        Object recuperado = _recuperarObjeto("clientes.dat");
        if(recuperado!=null){
            clientes = (Map<Integer,Cliente>) recuperado;
        }
    }
    public static void recuperarTodo(){
        recuperarUbicaciones();
        recuperarAlmacenes();
        recuperarHabitaciones();
        recuperarMobiliarios();
        recuperarClientes();
    }
    private static String _rutaArchivo(String archivo) throws IOException{
        File tempFile = File.createTempFile("temp", ".txt");
        String tempFilePath = tempFile.getAbsolutePath();
        tempFile.delete();
        String carpeta = tempFilePath.substring(0, tempFilePath.lastIndexOf(File.separator));
        //String carpeta = "/tmp";
        System.out.println(carpeta);
        Path rutaCompleta = Paths.get(carpeta, archivo);
        System.out.println(rutaCompleta);
        return rutaCompleta.toAbsolutePath().toString();
    }
    private static void _guardarObjeto(Object objeto,String filename){
        try {
            
            FileOutputStream archivoSalida = new FileOutputStream(_rutaArchivo(filename));
            ObjectOutputStream objetoSalida = new ObjectOutputStream(archivoSalida);
            objetoSalida.writeObject(objeto);
            objetoSalida.close();
            archivoSalida.close();
            System.out.println("La clase se ha guardado exitosamente en el archivo.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static Object _recuperarObjeto(String filename){
        try {
            FileInputStream archivoSalida = new FileInputStream(_rutaArchivo(filename));
            ObjectInputStream objetoSalida = new ObjectInputStream(archivoSalida);
            Object objetoRecuperado =  objetoSalida.readObject();
            objetoSalida.close();
            archivoSalida.close();
            System.out.println("Se recupero el archivo.");
            return objetoRecuperado;
        } catch (IOException | ClassNotFoundException e) {
            System.err.println(e.getMessage());            
        }
        return null;
    }
}
