/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.modelo;

import java.io.Serializable;

/**
 *
 * @author sistemas
 */
public class CodigoUnico implements Serializable {
    String codigoUnico;

    public CodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    public String getCodigoUnico() {
        return codigoUnico;
    }
    
}
