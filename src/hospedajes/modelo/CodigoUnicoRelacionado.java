/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.modelo;

import java.io.Serializable;

/**
 *
 * @author sistemas
 */
public class CodigoUnicoRelacionado extends CodigoUnico implements Serializable{
    String relacionCodigoUnico;

    public String getRelacionCodigoUnico() {
        return relacionCodigoUnico;
    }

    public CodigoUnicoRelacionado(String relacionCodigoUnico, String codigoUnico) {
        super(codigoUnico);
        this.relacionCodigoUnico = relacionCodigoUnico;
    }
    public CodigoUnicoRelacionado(CodigoUnico relacion, String codigoUnico) {
        super(codigoUnico);
        this.relacionCodigoUnico = relacion.getCodigoUnico();
    }
    
}
