/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.modelo;

import java.time.LocalDate;

/**
 *
 * @author sistemas
 */
public class HabitacionOcupaCliente {
    Habitacion habitacion;
    Cliente cliente;
    LocalDate fechaInicio;
    LocalDate fechaFin;
    int cantidad;
}
