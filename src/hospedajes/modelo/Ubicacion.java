/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.modelo;

import java.io.Serializable;

/**
 *
 * @author sistemas
 */
public class Ubicacion extends CodigoUnico implements Serializable {
    String direccion;
    public Ubicacion(String codigoUnico, String direccion) {
        super( codigoUnico);
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public String toString() {
      return codigoUnico + ":"+direccion;
    }
}
