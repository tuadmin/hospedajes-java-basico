/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.modelo;

import java.io.Serializable;

/**
 *
 * @author sistemas
 */
public class Almacen extends CodigoUnicoRelacionado implements Serializable{
    Ubicacion ubicacion;
    String descripcion;
    public Almacen(CodigoUnico relacion, String codigoUnico) {
        super(relacion, codigoUnico);
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
