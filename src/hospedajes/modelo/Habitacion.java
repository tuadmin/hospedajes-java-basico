package hospedajes.modelo;

import java.io.Serializable;
import java.util.Dictionary;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author sistemas
 */
public class Habitacion extends CodigoUnicoRelacionado implements Serializable{
    
    String titulo;
    String descripcion;
    double precioNoche;
    double precioHora;
    int piso;
    int lavamanos;
    int tasas;
    int duchas;
    int tinas;
    
    Ubicacion ubicacion;
    boolean disponible;
    public Habitacion(String titulo, String descripcion, double precioNoche, double precioHora, int piso, int lavamanos, int tasas, int duchas, int tinas, String relacionCodigoUnico, String codigoUnico) {
        super(relacionCodigoUnico, codigoUnico);
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.precioNoche = precioNoche;
        this.precioHora = precioHora;
        this.piso = piso;
        this.lavamanos = lavamanos;
        this.tasas = tasas;
        this.duchas = duchas;
        this.tinas = tinas;
    }

    
    public Habitacion(Ubicacion ubicacion,String titulo, String descripcion, double precioNoche, double precioHora, int piso, int lavamanos, int tasas, int duchas, int tinas, String codigoUnico) {
        super(ubicacion, codigoUnico);
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.precioNoche = precioNoche;
        this.precioHora = precioHora;
        this.piso = piso;
        this.lavamanos = lavamanos;
        this.tasas = tasas;
        this.duchas = duchas;
        this.tinas = tinas;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecioNoche() {
        return precioNoche;
    }

    public void setPrecioNoche(double precioNoche) {
        this.precioNoche = precioNoche;
    }

    public double getPrecioHora() {
        return precioHora;
    }

    public void setPrecioHora(double precioHora) {
        this.precioHora = precioHora;
    }

    public int getPiso() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso = piso;
    }

    public int getLavamanos() {
        return lavamanos;
    }

    public void setLavamanos(int lavamanos) {
        this.lavamanos = lavamanos;
    }

    public int getTasas() {
        return tasas;
    }

    public void setTasas(int tasas) {
        this.tasas = tasas;
    }

    public int getDuchas() {
        return duchas;
    }

    public void setDuchas(int duchas) {
        this.duchas = duchas;
    }

    public int getTinas() {
        return tinas;
    }

    public void setTinas(int tinas) {
        this.tinas = tinas;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }
    
    public String toString() {
      return codigoUnico + ":"+ titulo;
    }
}
