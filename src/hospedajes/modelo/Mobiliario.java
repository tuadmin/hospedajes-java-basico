/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.modelo;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author sistemas
 */
public class Mobiliario extends CodigoUnicoRelacionado implements Serializable{
    String descripcion;
    LocalDate fechaAdquisicion;
    double precioCompra;
    Ubicacion ubicacion;
    Habitacion habitacion;

    public Mobiliario(String descripcion, LocalDate fechaAdquisicion, double precioCompra, Ubicacion ubicacion, String codigoUnico) {
        super(ubicacion, codigoUnico);
        this.descripcion = descripcion;
        this.fechaAdquisicion = fechaAdquisicion;
        this.precioCompra = precioCompra;
        this.ubicacion = ubicacion;
    }

    public Mobiliario(String descripcion, double precioCompra, Habitacion habitacion, String codigoUnico) {
        super(habitacion,codigoUnico);
        this.descripcion = descripcion;
        this.precioCompra = precioCompra;
        this.habitacion = habitacion;
        this.ubicacion = this.habitacion.getUbicacion();
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDate getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(LocalDate fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public double getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(double precioCompra) {
        this.precioCompra = precioCompra;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Habitacion getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(Habitacion habitacion) {
        this.habitacion = habitacion;
    }
    public String toString() {
      return codigoUnico + ":"+descripcion;
    }
}
