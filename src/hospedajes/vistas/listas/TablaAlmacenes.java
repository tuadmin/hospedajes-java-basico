/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.vistas.listas;

import hospedajes.interfaces.InterfaceTablaVista;
import hospedajes.modelo.Almacen;
import hospedajes.modelo.Ubicacion;
import hospedajes.vistas.bases.BaseTablaVista;
import hospedajes.vistas.formularios.formularioAlmacen;
import hospedajes.vistas.formularios.formularioUbicacion;
import java.util.List;

/**
 *
 * @author v3ct0r
 */
public class TablaAlmacenes extends BaseTablaVista implements InterfaceTablaVista {
    public TablaAlmacenes() {        
        super();
        
    
    }
    @Override
    public void iniciar(){
        this.tieneAgregar=true;
        this.tieneBorrar=true;
        this.tieneEditar=false;
        super.iniciar();
        this.definirCabecera(new Object[]{ "Codigo","direccion","descripcion" },"nose");
        this.eventoBuscar("");
    }
    @Override
    public void eventoBuscar(String criterio){
        this.vaciarTabla();
        List<Almacen> lista = this.getOperacionesAlmacen().listar(criterio);
        for (Almacen almacen : lista) {            
            this.agregarFila(almacen, 
                    new Object[]{ almacen.getCodigoUnico(),almacen.getDescripcion(), almacen.getUbicacion().getDireccion() }
            );
        }
        
    }
    @Override
    public void eventoBorrar(Object fila){
        Ubicacion f = (Ubicacion) fila;
        Log.msgSuccess("se a borrado :" +f.getDireccion());
    }
    @Override
    public void eventoAgregar(){
        Padre.mostrarPopup(new formularioAlmacen());        
    }
}
