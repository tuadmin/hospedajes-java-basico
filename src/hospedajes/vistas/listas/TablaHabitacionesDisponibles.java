/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package hospedajes.vistas.listas;

import hospedajes.controlador.OperacionesAlmacen;
import hospedajes.controlador.OperacionesHabitacion;
import hospedajes.controlador.OperacionesMobiliario;
import hospedajes.controlador.OperacionesUbicacion;
import hospedajes.interfaces.InterfaceLista;
import hospedajes.interfaces.InterfaceTablaVista;
import hospedajes.interfaces.InterfaceUiLog;
import hospedajes.modelo.Habitacion;
import hospedajes.vistas.PantallaPrincipal;
import hospedajes.vistas.bases.BaseTablaVista;
import java.util.List;
import javax.swing.JButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author v3ct0r
 */
public class TablaHabitacionesDisponibles extends BaseTablaVista implements InterfaceTablaVista {
    public InterfaceUiLog Log;
    public PantallaPrincipal Padre;

    protected boolean tieneBuscar = true;
    protected boolean tieneEditar = true;
    protected boolean tieneAgregar = true;
    protected boolean tieneBorrar = true;
    
    /**
     * Creates new form Formulario
     */
    public TablaHabitacionesDisponibles() {
        initComponents();        
    }
    private DefaultTableModel _modeloTabla;
    public InterfaceLista funciones ;
    
    public void definirCabecera(Object[] cabecera,String botonAccion){
        Object[] nuevoArrayCabecera = new Object[cabecera.length + 1];
        System.arraycopy(cabecera, 0, nuevoArrayCabecera, 1, cabecera.length);
        nuevoArrayCabecera[0] = '_';
        _modeloTabla = new DefaultTableModel(nuevoArrayCabecera,0){
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 1) {
                    return JButton.class; // Indicar que la columna 3 contendrá botones
                } else {
                    return super.getColumnClass(columnIndex);
                }
            }
            @Override
            public boolean isCellEditable(int row, int column) {
                // Aquí puedes definir la lógica para controlar la editabilidad de las celdas individuales
                // En este ejemplo, la celda de la primera columna no es editable
                return column >2;
            }
           
        };
        jTable_datos.setDefaultRenderer(Object.class,new MiRendererListaHabitacion());
        jTable_datos.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                // Obtener el índice de la fila seleccionada
                int selectedRow = jTable_datos.getSelectedRow();

                if (selectedRow != -1) {
                    Habitacion habitacion = (Habitacion) _modeloTabla.getValueAt(selectedRow, 0);
                              
                    btnDesocupar.setEnabled(!habitacion.getDisponible());
                    btnOcupar.setEnabled(habitacion.getDisponible());
                    // Se ha seleccionado una fila, realizar la acción deseada
                    System.out.println("Se seleccionó la fila: " + selectedRow);

                    labelId.setText( _modeloTabla.getValueAt(selectedRow, 1)+"" );
                        
                }else{
                    btnOcupar.setEnabled(false);
                    btnDesocupar.setEnabled(false);
                    labelId.setText("");
                    
                }
            }
        });
        //jTable_datos.getModel().addTableModelListener(jTable_datos);
        jTable_datos.setModel(_modeloTabla);
        jTable_datos.getColumnModel().getColumn(0).setMaxWidth(0);
        jTable_datos.getColumnModel().getColumn(0).setMinWidth(0);
        jTable_datos.getColumnModel().getColumn(0).setWidth(0);
        //jTable_datos.getColumnModel().getColumn(1).setCellRenderer(new DibujarBoton());
        //TableCellRenderer tableRenderer = jTable_datos.getDefaultRenderer(JButton.class);
        //jTable_datos.setDefaultRenderer(JButton.class,new DibujarBoton(tableRenderer));
        //tabla.getColumnModel().getColumn(columnasVisibles.length).setMinWidth(0);
        //tabla.getColumnModel().getColumn(columnasVisibles.length).setMaxWidth(0);
        //tabla.getColumnModel().getColumn(columnasVisibles.length).setWidth(0);
    }
    public void vaciarTabla(){
        _modeloTabla.setRowCount(0);
    }
    public void agregarFila(Object[] columnas,String botonNombre){
        //Object[] nuevoArrayFila = new Object[columnas.length + 2];
        Object[] nuevoArrayFila = new Object[columnas.length + 1];
        if(botonNombre!=null){
            nuevoArrayFila[0] =  null;
            //nuevoArrayFila[1] =  new JButton(botonNombre);
        }
        System.arraycopy(columnas, 0, nuevoArrayFila, 1, columnas.length);
        _modeloTabla.addRow(nuevoArrayFila);
    }
    //public void agregarFila(Object fila,Object[] columnasVisibles,String botonNombre){
    public void agregarFila(Object fila,Object[] columnasVisibles){
        //Object[] nuevoArrayFila = new Object[columnasVisibles.length + 2];
        Object[] nuevoArrayFila = new Object[columnasVisibles.length + 1];
//        if(botonNombre!=null){
            nuevoArrayFila[0] = fila;
//            //nuevoArrayFila[1] =  crearBoton(botonNombre,this._modeloTabla,this);
//        }
        System.arraycopy(columnasVisibles, 0, nuevoArrayFila, 1, columnasVisibles.length);
        _modeloTabla.addRow(nuevoArrayFila);
        //_modeloTabla.setValueAt("sdasdas", 0, 2);
    }
    public void mostrarTabla(){
        
        
    }
    /*private static JButton crearBoton(final String texto,DefaultTableModel referencia ,final InterfaceTablaVista instancia) {
        JButton boton = new JButton(texto);
        boton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                //System.out.println("Botón en la fila " + fila + " presionado.");
                //instancia.eventoAccionFila(referencia.getValueAt(0, 0));
            }
        });
        return boton;
    }/**/

//    public OperacionesUbicacion getOperacionesUbicacion() {
//        return operacionesUbicacion;
//    }
//
//    public void setOperacionesUbicacion(OperacionesUbicacion operacionesUbicacion) {
//        this.operacionesUbicacion = operacionesUbicacion;
//    }
//
//    public OperacionesAlmacen getOperacionesAlmacen() {
//        return operacionesAlmacen;
//    }
//
//    public void setOperacionesAlmacen(OperacionesAlmacen operacionesAlmacen) {
//        this.operacionesAlmacen = operacionesAlmacen;
//    }
//
//    public OperacionesHabitacion getOperacionesHabitacion() {
//        return operacionesHabitacion;
//    }
//
//
//    public OperacionesMobiliario getOperacionesMobiliario() {
//        return operacionesMobiliario;
//    }
//
//    public void setOperacionesMobiliario(OperacionesMobiliario operacionesMobiliario) {
//        this.operacionesMobiliario = operacionesMobiliario;
//    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_datos = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnBuscar = new javax.swing.JButton();
        inBuscar = new javax.swing.JTextField();
        btnAgregar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btnDesocupar = new javax.swing.JButton();
        labelId = new javax.swing.JLabel();
        btnOcupar = new javax.swing.JButton();

        setToolTipText("");

        jTable_datos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Descripcion", "Precio X hora", "Precio X Noche", "estado?"
            }
        ));
        jScrollPane1.setViewportView(jTable_datos);

        btnBuscar.setText("🔍 buscar");
        btnBuscar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBuscar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        inBuscar.setToolTipText("");
        inBuscar.setCaretColor(new java.awt.Color(0, 0, 153));

        btnAgregar.setBackground(new java.awt.Color(51, 153, 255));
        btnAgregar.setForeground(new java.awt.Color(255, 255, 255));
        btnAgregar.setText("+Agregar");
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(inBuscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnBuscar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                            .addComponent(inBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
        );

        jPanel2.setLayout(new java.awt.GridLayout(1, 4, 2, 2));

        btnDesocupar.setBackground(new java.awt.Color(255, 255, 204));
        btnDesocupar.setText("DESOCUPAR");
        btnDesocupar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesocuparActionPerformed(evt);
            }
        });
        jPanel2.add(btnDesocupar);

        labelId.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelId.setText("0");
        jPanel2.add(labelId);

        btnOcupar.setBackground(new java.awt.Color(255, 153, 153));
        btnOcupar.setText("OCUPAR");
        btnOcupar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOcuparActionPerformed(evt);
            }
        });
        jPanel2.add(btnOcupar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(2, 2, 2))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnDesocuparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesocuparActionPerformed
        int fila = jTable_datos.getSelectedRow();        
        this.eventoDesocupar(_modeloTabla.getValueAt(fila, 0));
        this.eventoBuscar(inBuscar.getText().trim());
    }//GEN-LAST:event_btnDesocuparActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        this.eventoBuscar(inBuscar.getText().trim());
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnOcuparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOcuparActionPerformed
        int fila = jTable_datos.getSelectedRow();
        //Object test = _modeloTabla.getValueAt(0, 0);
        //Object test2 = _modeloTabla.getValueAt(0, 1);
        this.eventoOcupar(_modeloTabla.getValueAt(fila, 0));
        this.eventoBuscar(inBuscar.getText().trim());
    }//GEN-LAST:event_btnOcuparActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        this.eventoAgregar();
    }//GEN-LAST:event_btnAgregarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnDesocupar;
    private javax.swing.JButton btnOcupar;
    private javax.swing.JTextField inBuscar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable_datos;
    private javax.swing.JLabel labelId;
    // End of variables declaration//GEN-END:variables



    @Override
    public void iniciar() {
        this.tieneAgregar=false;
        this.tieneBorrar=true;
        this.tieneEditar=false;

        this.definirCabecera(new Object[]{ "Codigo","titulo","descripcion","precio X hora","precio X noche", "disponible" },"nose");
        this.eventoBuscar("");
        btnAgregar.setEnabled(tieneAgregar);
        btnOcupar.setEnabled(false);
        btnDesocupar.setEnabled(false);
        //if(!tieneBuscar){
            inBuscar.setEnabled(tieneBuscar);
        //}
        btnBuscar.setEnabled(tieneBuscar);
    }
    @Override
    public void eventoBuscar(String criterio) {
        
        this.vaciarTabla();
        List<Habitacion> lista = this.getOperacionesHabitacion().listar(criterio);
        for (Habitacion habitacion : lista) {            
            this.agregarFila(habitacion, 
                    new Object[]{ habitacion.getCodigoUnico(),habitacion.getTitulo(),
                    habitacion.getDescripcion(), habitacion.getPrecioHora(), habitacion.getPrecioNoche(), 
                    habitacion.getDisponible()?"LIBRE":"OCUPADO" }
            );
        }
    }

    @Override
    public void eventoAgregar() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void eventoEditar(Object fila) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void eventoBorrar(Object fila) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void eventoDesocupar(Object valueAt) {
        Habitacion h = (Habitacion) valueAt;
        h.setDisponible(true);
        this.getOperacionesHabitacion().modificar(h);
    }

    private void eventoOcupar(Object valueAt) {
        Habitacion h = (Habitacion) valueAt;
        h.setDisponible(false);
        
        this.getOperacionesHabitacion().modificar(h);
    }
}
