/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package hospedajes.vistas.listas;

import hospedajes.controlador.OperacionesAlmacen;
import hospedajes.controlador.OperacionesHabitacion;
import hospedajes.controlador.OperacionesUbicacion;
import hospedajes.vistas.formularios.*;
import hospedajes.interfaces.InterfaceFormulario;
import hospedajes.interfaces.InterfaceLista;
import hospedajes.interfaces.InterfaceTablaVista;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author v3ct0r
 */
public class TablaVista extends javax.swing.JPanel implements InterfaceTablaVista {
    OperacionesUbicacion operacionesUbicacion;
    OperacionesAlmacen operacionesAlmacen;
    OperacionesHabitacion operacionesHabitacion;
    /**
     * Creates new form Formulario
     */
    public TablaVista() {
        initComponents();
    }
    private DefaultTableModel _modeloTabla;
    public InterfaceLista funciones ;
    
    public void definirCabecera(Object[] cabecera,String botonAccion){
        Object[] nuevoArrayCabecera = new Object[cabecera.length + 1];
        System.arraycopy(cabecera, 0, nuevoArrayCabecera, 1, cabecera.length);
        nuevoArrayCabecera[0] = '_';
        _modeloTabla = new DefaultTableModel(nuevoArrayCabecera,0);
        
        //tabla.getColumnModel().getColumn(columnasVisibles.length).setMinWidth(0);
        //tabla.getColumnModel().getColumn(columnasVisibles.length).setMaxWidth(0);
        //tabla.getColumnModel().getColumn(columnasVisibles.length).setWidth(0);
    }
    public void vaciarTabla(){
        _modeloTabla.setRowCount(0);
    }
    public void agregarFila(Object[] columnas,String botonNombre){
        Object[] nuevoArrayFila = new Object[columnas.length + 2];
        if(botonNombre!=null){
            nuevoArrayFila[0] =  null;
            nuevoArrayFila[1] =  new JButton(botonNombre);
        }
        _modeloTabla.addRow(nuevoArrayFila);
    }
    public void agregarFila(Object fila,Object[] columnasVisibles,String botonNombre){
        Object[] nuevoArrayFila = new Object[columnasVisibles.length + 2];
        if(botonNombre!=null){
            nuevoArrayFila[0] = fila;
            nuevoArrayFila[0] =  crearBoton(botonNombre,this._modeloTabla,this);
        }
        System.arraycopy(columnasVisibles, 0, nuevoArrayFila, 1, columnasVisibles.length);
        _modeloTabla.addRow(nuevoArrayFila);
    }
    public void mostrarTabla(){
        jTable_datos.setModel(_modeloTabla);
        jTable_datos.getColumnModel().getColumn(0).setMaxWidth(0);
        jTable_datos.getColumnModel().getColumn(0).setMinWidth(0);
        jTable_datos.getColumnModel().getColumn(0).setWidth(0);
        
    }
    private static JButton crearBoton(final String texto,DefaultTableModel referencia ,final InterfaceTablaVista instancia) {
        JButton boton = new JButton(texto);
        boton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                //System.out.println("Botón en la fila " + fila + " presionado.");
                //instancia.eventoAccionFila(referencia.getValueAt(0, 0));
            }
        });
        return boton;
    }

    public OperacionesUbicacion getOperacionesUbicacion() {
        return operacionesUbicacion;
    }

    public void setOperacionesUbicacion(OperacionesUbicacion operacionesUbicacion) {
        this.operacionesUbicacion = operacionesUbicacion;
    }

    public OperacionesAlmacen getOperacionesAlmacen() {
        return operacionesAlmacen;
    }

    public void setOperacionesAlmacen(OperacionesAlmacen operacionesAlmacen) {
        this.operacionesAlmacen = operacionesAlmacen;
    }

    public OperacionesHabitacion getOperacionesHabitacion() {
        return operacionesHabitacion;
    }

    public void setOperacionesHabitacion(OperacionesHabitacion operacionesHabitacion) {
        this.operacionesHabitacion = operacionesHabitacion;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_botones = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        inBuscar = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_datos = new javax.swing.JTable();

        setToolTipText("");

        jPanel_botones.setLayout(new javax.swing.BoxLayout(jPanel_botones, javax.swing.BoxLayout.LINE_AXIS));

        jToolBar1.setRollover(true);

        inBuscar.setToolTipText("");
        inBuscar.setCaretColor(new java.awt.Color(0, 0, 153));
        jToolBar1.add(inBuscar);

        btnBuscar.setText("🔍 buscar");
        btnBuscar.setFocusable(false);
        btnBuscar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBuscar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(btnBuscar);

        jPanel_botones.add(jToolBar1);

        jTable_datos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable_datos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel_botones, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel_botones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JTextField inBuscar;
    private javax.swing.JPanel jPanel_botones;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable_datos;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void eventoBuscar(String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void eventoBorrar(Object fila) {
        
    }

    @Override
    public void iniciar() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void eventoAgregar() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void eventoEditar(Object fila) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
