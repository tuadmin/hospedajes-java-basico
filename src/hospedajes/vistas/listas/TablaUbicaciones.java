/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.vistas.listas;

import hospedajes.interfaces.InterfaceTablaVista;
import hospedajes.modelo.Ubicacion;
import hospedajes.vistas.bases.BaseTablaVista;
import hospedajes.vistas.formularios.formularioUbicacion;
import java.util.List;

/**
 *
 * @author v3ct0r
 */
public class TablaUbicaciones extends BaseTablaVista implements InterfaceTablaVista {
    public TablaUbicaciones() {        
        super();
        
    
    }
    @Override
    public void iniciar(){
        this.tieneAgregar=true;
        this.tieneBorrar=true;
        this.tieneEditar=false;
        super.iniciar();
        this.definirCabecera(new Object[]{ "Codigo","direccion" },"nose");
        this.eventoBuscar("");
    }
    @Override
    public void eventoBuscar(String criterio){
        this.vaciarTabla();
        List<Ubicacion> lista = this.getOperacionesUbicacion().listar(criterio);
        for (Ubicacion ubicacion : lista) {            
            this.agregarFila(ubicacion, 
                    new Object[]{ ubicacion.getCodigoUnico(),ubicacion.getDireccion() }
            );
        }
        
    }
    @Override
    public void eventoBorrar(Object fila){
        Ubicacion f = (Ubicacion) fila;
        Log.msgSuccess("se a borrado :" +f.getDireccion());
    }
    @Override
    public void eventoAgregar(){
        Padre.mostrarPopup(new formularioUbicacion());        
    }
}
