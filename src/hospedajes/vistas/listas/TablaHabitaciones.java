/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.vistas.listas;

import hospedajes.interfaces.InterfaceTablaVista;
import hospedajes.modelo.Habitacion;
import hospedajes.vistas.bases.BaseTablaVista;
import hospedajes.vistas.formularios.formularioHabitacion;
import hospedajes.vistas.formularios.formularioUbicacion;
import java.util.List;

/**
 *
 * @author v3ct0r
 */
public class TablaHabitaciones extends BaseTablaVista implements InterfaceTablaVista {
    public TablaHabitaciones() {        
        super();
        
    
    }
    @Override
    public void iniciar(){
        this.tieneAgregar=true;
        this.tieneBorrar=true;
        this.tieneEditar=false;
        super.iniciar();
        this.definirCabecera(new Object[]{ "Codigo","titulo","descripcion","precio X hora", "precio X noche" },"nose");
        this.eventoBuscar("");
    }
    @Override
    public void eventoBuscar(String criterio){
        this.vaciarTabla();
        List<Habitacion> lista = this.getOperacionesHabitacion().listar(criterio);
        for (Habitacion habitacion : lista) {            
            this.agregarFila(habitacion, 
                    new Object[]{ habitacion.getCodigoUnico(),habitacion.getTitulo(),
                    habitacion.getDescripcion(), habitacion.getPrecioHora(), habitacion.getPrecioNoche()}
            );
        }
        
    }
    @Override
    public void eventoBorrar(Object fila){
        Habitacion f = (Habitacion) fila;
        Log.msgSuccess("se a borrado :" +f.getCodigoUnico());
    }
    @Override
    public void eventoAgregar(){
        Padre.mostrarPopup(new formularioHabitacion());        
    }
}