/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.vistas.listas;

import hospedajes.interfaces.InterfaceTablaVista;
import hospedajes.modelo.Habitacion;
import hospedajes.modelo.Mobiliario;
import hospedajes.vistas.bases.BaseTablaVista;
import hospedajes.vistas.formularios.formularioHabitacion;
import hospedajes.vistas.formularios.formularioMobiliario;
import hospedajes.vistas.formularios.formularioUbicacion;
import java.util.List;

/**
 *
 * @author v3ct0r
 */
public class TablaMobiliarios extends BaseTablaVista implements InterfaceTablaVista {
    public TablaMobiliarios() {        
        super();
        
    
    }
    @Override
    public void iniciar(){
        this.tieneAgregar=true;
        this.tieneBorrar=true;
        this.tieneEditar=false;
        super.iniciar();
        this.definirCabecera(new Object[]{ "Codigo","descripcion","precio Compra", "habitacion" },"nose");
        this.eventoBuscar("");
    }
    @Override
    public void eventoBuscar(String criterio){
        this.vaciarTabla();
        List<Mobiliario> lista = this.getOperacionesMobiliario().listar(criterio);
        for (Mobiliario mobi : lista) {            
            this.agregarFila(mobi, 
                    new Object[]{ mobi.getCodigoUnico(),
                    mobi.getDescripcion(), mobi.getPrecioCompra(), mobi.getHabitacion().getCodigoUnico()}
            );
        }
        
    }
    @Override
    public void eventoBorrar(Object fila){
        Mobiliario f = (Mobiliario) fila;
        Log.msgSuccess("se a borrado :" +f.getCodigoUnico());
    }
    @Override
    public void eventoAgregar(){
        Padre.mostrarPopup(new formularioMobiliario());        
    }
}