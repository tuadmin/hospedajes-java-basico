/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JInternalFrame.java to edit this template
 */
package hospedajes.vistas.bases;

import hospedajes.controlador.OperacionesAlmacen;
import hospedajes.controlador.OperacionesHabitacion;
import hospedajes.controlador.OperacionesMobiliario;
import hospedajes.controlador.OperacionesUbicacion;
import hospedajes.interfaces.InterfaceFormulario;
import hospedajes.interfaces.InterfaceUiLog;
import hospedajes.vistas.formularios.Formulario;
import hospedajes.vistas.listas.TablaVista;
import java.awt.BorderLayout;
import java.awt.Color;


/**
 *
 * @author v3ct0r
 */
public class BaseInternalFrameVista extends javax.swing.JInternalFrame implements InterfaceUiLog {
    private static Color plomoClaro = new Color(242,242,242);
    OperacionesUbicacion operacionesUbicacion;
    OperacionesAlmacen operacionesAlmacen;
    OperacionesHabitacion operacionesHabitacion;
    OperacionesMobiliario operacionesMobiliario;
    /**
     * Creates new form BaseInternalFrame
     */
    public BaseInternalFrameVista() {
        initComponents();
        //this.add(new BaseConfiguracionVista());
    }
    public BaseInternalFrameVista( BaseFormularioPanel form) {
        initComponents();
    }
    @Override
    public void msgClean(){
        jLabel_MSG.setText("");
    }
    @Override
    public void msgSuccess(String text){
        jLabel_MSG.setForeground( new Color(0, 102, 0));
        jLabel_MSG.setBackground(plomoClaro);
        jLabel_MSG.setText(text);
    }
    @Override
    public void msgWarning(String text){
        jLabel_MSG.setForeground(Color.yellow);
        jLabel_MSG.setBackground(Color.black);
        
        jLabel_MSG.setText("/!\\"+text);
    }
    @Override
    public void msgError(String text){
        jLabel_MSG.setForeground(Color.red);
        jLabel_MSG.setBackground(plomoClaro);
        jLabel_MSG.setText("( X )"+text);
    }
    @Override
    public void msgInfo(String text){
        jLabel_MSG.setForeground(Color.blue);
        jLabel_MSG.setBackground(plomoClaro);
        jLabel_MSG.setText(text);
    }
    public void  agregarPanel(BaseFormularioPanel form) {
        jPanel_container.setLayout(new BorderLayout());
        form.Log = this;
        form.operacionesUbicacion = this.operacionesUbicacion;
        form.operacionesAlmacen = this.operacionesAlmacen;
        form.operacionesHabitacion = this.operacionesHabitacion;
        form.operacionesMobiliario = this.operacionesMobiliario;
        Formulario contenedor= new Formulario();
        contenedor.insertar(form);
        if(form instanceof InterfaceFormulario){
            ((InterfaceFormulario)form).iniciar();
        }        
        jPanel_container.add(contenedor);                
    }
    public void  agregarPanel(BaseTablaVista tabla) {
        jPanel_container.setLayout(new BorderLayout());
        tabla.Log = this;
        tabla.operacionesUbicacion = this.operacionesUbicacion;
        tabla.operacionesAlmacen = this.operacionesAlmacen;
        tabla.operacionesHabitacion = this.operacionesHabitacion;
        tabla.operacionesMobiliario = this.operacionesMobiliario;
        tabla.iniciar();
        jPanel_container.add(tabla);                
    }
    public OperacionesUbicacion getOperacionesUbicacion() {
        return operacionesUbicacion;
    }

    public void setOperacionesUbicacion(OperacionesUbicacion operacionesUbicacion) {
        this.operacionesUbicacion = operacionesUbicacion;
    }

    public OperacionesAlmacen getOperacionesAlmacen() {
        return operacionesAlmacen;
    }

    public void setOperacionesAlmacen(OperacionesAlmacen operacionesAlmacen) {
        this.operacionesAlmacen = operacionesAlmacen;
    }

    public OperacionesHabitacion getOperacionesHabitacion() {
        return operacionesHabitacion;
    }

    public void setOperacionesHabitacion(OperacionesHabitacion operacionesHabitacion) {
        this.operacionesHabitacion = operacionesHabitacion;
    }

    public OperacionesMobiliario getOperacionesMobiliario() {
        return operacionesMobiliario;
    }

    public void setOperacionesMobiliario(OperacionesMobiliario operacionesMobiliario) {
        this.operacionesMobiliario = operacionesMobiliario;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel_titulo = new javax.swing.JLabel();
        jPanel_container = new javax.swing.JPanel();
        jLabel_MSG = new javax.swing.JLabel();

        setClosable(true);
        setMaximizable(true);
        setResizable(true);

        jLabel_titulo.setFont(new java.awt.Font("Helvetica Neue", 1, 18)); // NOI18N
        jLabel_titulo.setForeground(new java.awt.Color(0, 0, 255));
        jLabel_titulo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel_titulo.setText(" ");

        jPanel_container.setBackground(new java.awt.Color(204, 255, 204));

        javax.swing.GroupLayout jPanel_containerLayout = new javax.swing.GroupLayout(jPanel_container);
        jPanel_container.setLayout(jPanel_containerLayout);
        jPanel_containerLayout.setHorizontalGroup(
            jPanel_containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel_containerLayout.setVerticalGroup(
            jPanel_containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 263, Short.MAX_VALUE)
        );

        jLabel_MSG.setBackground(new java.awt.Color(255, 255, 255));
        jLabel_MSG.setForeground(new java.awt.Color(255, 255, 0));
        jLabel_MSG.setText(" ");
        jLabel_MSG.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel_MSG.setFocusable(false);
        jLabel_MSG.setOpaque(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel_MSG, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel_titulo, javax.swing.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel_container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel_titulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel_container, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_MSG))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel_MSG;
    private javax.swing.JLabel jLabel_titulo;
    private javax.swing.JPanel jPanel_container;
    // End of variables declaration//GEN-END:variables
}
