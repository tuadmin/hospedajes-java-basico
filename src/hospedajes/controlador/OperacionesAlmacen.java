package hospedajes.controlador;

import hospedajes.modelo.Almacen;
import hospedajes.modelo.Ubicacion;
import java.util.ArrayList;
import java.util.List;
import repositorio.Persistencia;

public class OperacionesAlmacen {
    public Almacen agregar(Ubicacion ubicacion, String codigoUnico){        
        Almacen nuevo = new Almacen(ubicacion, codigoUnico);
        if (Persistencia.almacenes.containsKey(nuevo.getCodigoUnico()))
            return null;
        //nuevo.setUbicacion(ubicacion);
        nuevo.setUbicacion(ubicacion);
        Persistencia.almacenes.put(nuevo.getCodigoUnico(), nuevo);
        Persistencia.guardarAlmacenes();
        return nuevo;
    }
    public Almacen buscar(String codigoUnico){
        if (Persistencia.almacenes.containsKey(codigoUnico))
            return Persistencia.almacenes.get(codigoUnico);
        return null;
    }
    public Almacen modificar(String codigoUnico, Ubicacion ubicacion){
        if (Persistencia.almacenes.containsKey(codigoUnico)){
            Almacen almacen = Persistencia.almacenes.get(codigoUnico);
            almacen.setUbicacion(ubicacion);
            Persistencia.guardarAlmacenes();
            return almacen;
        }
        return null;
    }
    public boolean eliminar(String codigoUnico){
        if (Persistencia.almacenes.containsKey(codigoUnico)){
            Persistencia.almacenes.remove(codigoUnico);
            Persistencia.guardarAlmacenes();
            return true;
        }
        return false;
    }

    public List<Almacen> listar(String criterio){
        List<Almacen> encontrados = new ArrayList<>();
        if(!Persistencia.ubicaciones.isEmpty()){            
            for (Almacen encontrado : Persistencia.almacenes.values()) {
                if(encontrado.getUbicacion() == null){
                    encontrado.setUbicacion(Persistencia.ubicaciones.get(encontrado.getRelacionCodigoUnico()));
                }
                if(encontrado.getCodigoUnico().contains(criterio)
                || encontrado.getDescripcion().contains(criterio) ){
                    encontrados.add(encontrado);
                }
            }
            
        }
        return encontrados;       
    }
}
