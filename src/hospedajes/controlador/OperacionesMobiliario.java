/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.controlador;

import hospedajes.modelo.Habitacion;
import hospedajes.modelo.Mobiliario;
import hospedajes.modelo.Ubicacion;
import java.beans.PersistenceDelegate;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import repositorio.Persistencia;

/**
 *
 * @author v3ct0r
 */
public class OperacionesMobiliario {
    public Mobiliario agregar(Habitacion habitacion, String descripcion, double precioCompra, String codigoUnico){
        Mobiliario nuevo = new Mobiliario(descripcion, precioCompra, habitacion, codigoUnico);
        if (Persistencia.mobiliarios.containsKey(nuevo.getCodigoUnico()))
            return null;
        Persistencia.mobiliarios.put(nuevo.getCodigoUnico(), nuevo);
        Persistencia.guardarMobiliarios();
        return nuevo;
    }
    public Mobiliario buscar(String codigoUnico){
        if (Persistencia.mobiliarios.containsKey(codigoUnico))
            return Persistencia.mobiliarios.get(codigoUnico);
        return null;
    }
    public Mobiliario modificar( Mobiliario mobinuevo){
        if (Persistencia.mobiliarios.containsKey(mobinuevo.getCodigoUnico())){
            
            Persistencia.mobiliarios.replace(mobinuevo.getCodigoUnico(), mobinuevo);
            Persistencia.guardarUbicaciones();
            return mobinuevo;
        }
        return null;
    }
    public boolean eliminar(String codigoUnico){
        if (Persistencia.mobiliarios.containsKey(codigoUnico)){
            Persistencia.mobiliarios.remove(codigoUnico);
            Persistencia.guardarUbicaciones();
            return true;
        }
        return false;
    }

    public List<Mobiliario> listar(String criterio){
        List<Mobiliario> encontrados = new ArrayList<>();
        if(!Persistencia.mobiliarios.isEmpty()){            
            for (Mobiliario encontrado : Persistencia.mobiliarios.values()) {
                if(encontrado.getCodigoUnico().contains(criterio)
                || encontrado.getDescripcion().contains(criterio) ){
                    if(encontrado.getHabitacion() == null){
                        encontrado.setHabitacion(  Persistencia.habitaciones.get(encontrado.getRelacionCodigoUnico()) );
                        encontrado.setUbicacion(encontrado.getHabitacion().getUbicacion());
                    }
                    encontrados.add(encontrado);
                }
            }
            
        }
        return encontrados;       
    }
}
