/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.controlador;

import hospedajes.modelo.Ubicacion;
import java.util.List;
import java.util.ArrayList;
import repositorio.Persistencia;

/**
 *
 * @author v3ct0r
 */
public class OperacionesUbicacion {
    public Ubicacion agregar(String codigoUnico, String direccion){
        Ubicacion nuevo = new Ubicacion(codigoUnico,direccion);
        if (Persistencia.ubicaciones.containsKey(nuevo.getCodigoUnico()))
            return null;
        Persistencia.ubicaciones.put(nuevo.getCodigoUnico(), nuevo);
        Persistencia.guardarUbicaciones();
        return nuevo;
    }
    public Ubicacion buscar(String codigoUnico){
        if (Persistencia.ubicaciones.containsKey(codigoUnico))
            return Persistencia.ubicaciones.get(codigoUnico);
        return null;
    }
    public Ubicacion modificar(String codigoUnico, String direccion){
        if (Persistencia.ubicaciones.containsKey(codigoUnico)){
            Ubicacion ubicacion = Persistencia.ubicaciones.get(codigoUnico);
            ubicacion.setDireccion(direccion);
            Persistencia.guardarUbicaciones();
            return ubicacion;
        }
        return null;
    }
    public boolean eliminar(String codigoUnico){
        if (Persistencia.ubicaciones.containsKey(codigoUnico)){
            Persistencia.ubicaciones.remove(codigoUnico);
            Persistencia.guardarUbicaciones();
            return true;
        }
        return false;
    }
    public Ubicacion[] listar(){
        if(Persistencia.ubicaciones.size()>0){
            return Persistencia.ubicaciones.values().toArray(new Ubicacion[0]);
        }
        return new Ubicacion[0];        
    }
    public List<Ubicacion> listar(String criterio){
        List<Ubicacion> encontrados = new ArrayList<>();
        if(!Persistencia.ubicaciones.isEmpty()){            
            for (Ubicacion encontrado : Persistencia.ubicaciones.values()) {
                if(encontrado.getCodigoUnico().contains(criterio)
                || encontrado.getDireccion().contains(criterio) ){
                    encontrados.add(encontrado);
                }
            }
            
        }
        return encontrados;       
    }
}
