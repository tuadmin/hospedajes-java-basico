/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospedajes.controlador;

import hospedajes.modelo.Almacen;
import hospedajes.modelo.Habitacion;
import hospedajes.modelo.Ubicacion;
import java.util.ArrayList;
import java.util.List;
import repositorio.Persistencia;

/**
 *
 * @author sistemas
 */
public class OperacionesHabitacion {
    //Persistencia persistencia;
    public Habitacion agregar(Ubicacion ubicacion, String titulo, String descripcion, double precioNoche, double precioHora, int piso, int lavamanos, int tasas, int duchas, int tinas,String codigoUnico){
        Habitacion nuevo = new Habitacion(ubicacion, titulo,  descripcion,  precioNoche,  precioHora, piso, lavamanos, tasas, duchas, tinas,  codigoUnico);
        
        //nuevo.setUbicacion(ubicacion);
        nuevo.setUbicacion(ubicacion);
        
        if (Persistencia.habitaciones.containsKey(nuevo.getCodigoUnico()))
            return null;
        
        Persistencia.habitaciones.put(nuevo.getCodigoUnico(), nuevo);
        Persistencia.guardarHabitaciones();
        return nuevo;
        //Persistencia.habitaciones.
    }
    public List<Habitacion> listar(String criterio){
        List<Habitacion> encontrados = new ArrayList<>();
        if(!Persistencia.ubicaciones.isEmpty()){            
            for (Habitacion encontrado : Persistencia.habitaciones.values()) {
                if(encontrado.getUbicacion() == null){
                    encontrado.setUbicacion(Persistencia.ubicaciones.get(encontrado.getRelacionCodigoUnico()));
                }
                if(encontrado.getCodigoUnico().contains(criterio)
                || encontrado.getDescripcion().contains(criterio)
                || encontrado.getTitulo().contains(criterio)
                        
                        ){
                    encontrados.add(encontrado);
                }
            }
            
        }
        return encontrados;       
    }
    public Habitacion obtener(String codigoUnico){
        Habitacion encontrado = null;
        if(!Persistencia.ubicaciones.isEmpty()){
            encontrado= Persistencia.habitaciones.get(codigoUnico);            
        }
        return encontrado;       
    }
    public Habitacion modificar(Habitacion habitacion){
        if (Persistencia.habitaciones.containsKey(habitacion.getCodigoUnico())){
            Persistencia.habitaciones.replace(habitacion.getCodigoUnico(), habitacion);
            Persistencia.guardarHabitaciones();
            return habitacion;
        }
        return null;
    }
}
