/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hospedajes.interfaces;

/**
 *
 * @author v3ct0r
 */
public interface InterfaceUiLog {
    public void msgClean();
    public void msgSuccess(String text);
    public void msgWarning(String text);
    public void msgError(String text);
    public void msgInfo(String text);
}
