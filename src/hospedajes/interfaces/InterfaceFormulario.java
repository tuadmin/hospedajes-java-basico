/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hospedajes.interfaces;

/**
 *
 * @author v3ct0r
 */
public interface InterfaceFormulario {
    public void iniciar();
    public void guardar();
    public void limpiar();
    //public void editar();
}
