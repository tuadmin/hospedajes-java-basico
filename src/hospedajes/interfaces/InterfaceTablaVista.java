/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hospedajes.interfaces;

/**
 *
 * @author v3ct0r
 */
public interface InterfaceTablaVista {
    public void iniciar();
    public void eventoBuscar(String criterio);
    //public Object[][] lista();
    public void eventoAgregar();
    public void eventoEditar(Object fila);
    public void eventoBorrar(Object fila);
}
